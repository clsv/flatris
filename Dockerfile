FROM node
RUN mkdir /app
WORKDIR /app
COPY flatris/package.json /app
RUN yarn install
COPY flatris/ /app
RUN yarn build
CMD [ "yarn", "start" ]
EXPOSE 3000/tcp
